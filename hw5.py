""" Homework 5
-- Due Sunday, Feb. 22th at 23:59
-- Always write the final code yourself
-- Cite any websites you referenced
-- Use the PEP-8 checker for full style points:
https://pypi.python.org/pypi/pep8
"""


class LogFileManager(object):
    '''Write a context manager which ensures that file objects are closed.
    Additionally, log out to a file called file_io_log.txt. The log
    should record the command used to open the file as well as any
    errors.  The format of the log should be
    'open(...) -> Exception: None\n' if there were no errors and
    if an error occurs record the details of the error after 'Exception:'
    'open(...) -> Exception: <class 'io.UnsupportedOperation'>: not writable\n'
    Here's the PEP on with statments https://www.python.org/dev/peps/pep-0343/
    Search for 'The translation of the above' on that page for a good
    explanation of the with statement
    '''
    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class Leibniz(object):
    '''Write a custom generator class for the Leibniz infinite series,
    which converges to pi/4.
    See here: http://en.wikipedia.org/wiki/Leibniz_series

    Figure out which magic method(s) you need to implement.
    In addition, write a method list_leibniz(n) that returns a list of
    the first n terms of the series.
    '''
    def list_leibniz(self, n):
        pass


class InvalidFormatException(Exception):
    ''' For use below. '''
    pass


def csv_to_dict(infile):
    ''' Convert a csv file to a list of dictionaries, and return the list.
    For instance, a file containing:
    student,age,gender
    Anne,22,female
    John,21,male

    should be converted to the list:
    [{'student': 'Anne', 'age': 22, 'gender': 'female'},
     {'student': 'John', 'age': 21, 'gender': 'male'}]

    -- If the csvfile (i.e. the argument "infile") does not exist, catch
       the FileNotFoundError, and raise a new exception with the message
       "No such file"
    -- You may assume that a single comma separates each column.
    -- If the file is properly formatted, the number of columns in each row
       will the be same. If this is not the case,
       raise an InvalidFormatException
    -- You may assume that the first row of the csv file contains field names.

    Do not use the DictReader method of the csv module -- your job is to
    implement this behavior yourself.
    '''
    pass


def encode_text(codefile, infile, outfile):
    ''' Encode the text in infile according to the mappings in codefile,
    and write the result to outfile. If either infile or codefile
    does not exist, catch the I/O exception, and raise a new exception
    with the message "No such file".

    The codefile will contain mappings, one per line, of one character
    to another: for instance, a,b means that 'a' should be mapped to 'b'
    in the encoded string. If a character is not mapped in the codefile,
    it should not be changed in the output.
    Look at dir('') for useful functions

    If any line in the input file doesn't match this format, or if one
    character is mapped to multiple other characters, raise an
    InvalidFormatException.
    '''
    pass


def decode_text(codefile, infile, outfile):
    '''Decode the text in infile, assuming that it was encoded
    according to the mapping in codefile. Write the result to outfile.
    If either infile or codefile does notexist,
    use the same procedure as before.

    Keep your code DRY (Don't Repeat Yourself). Feel free to write
    auxiliary functions that will be called by both encode_text
    and decode_text.
    '''
    pass


def main():
    pass


if __name__ == "__main__":
    main()
