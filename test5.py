import unittest
import math
from hw5 import *


class TestHomework5(unittest.TestCase):

    def setUp(self):
        pass

    # Log File Manager
    def test_LogFileManager(self):
        LOG_FILE = 'file_io_log.txt'
        JUNK_FILE = 'junk_file.junk'
        JUNK_LINES = ['line1\n', 'line2\n']

        with open(LOG_FILE, 'w'):
            pass  # truncate the log file

        with LogFileManager(JUNK_FILE, 'w') as f1:
            f1.writelines(JUNK_LINES)

        self.assertTrue(f1.closed)

        with open(JUNK_FILE) as f2:
            self.assertEqual(f2.read(), ''.join(JUNK_LINES))

        with open(LOG_FILE) as f3:
            output = "open({!r}, 'w') -> Exception: None\n".format(JUNK_FILE)
            self.assertEqual(f3.read(), output)

        try:
            with LogFileManager(JUNK_FILE, 'r+t'):
                [][2]
        except IndexError:
            ind_err = "<class 'IndexError'>: list index out of range"
            with open(LOG_FILE) as f4:
                f_str = "open({!r}, 'r+t') -> Exception: {}\n"
                output = f_str.format(JUNK_FILE, ind_err)
                self.assertEqual(f4.readlines()[1], output)
        else:
            self.fail('Did not raise index error')

    # Leibniz
    def test_Leibniz(self):
        l = Leibniz()
        g = iter(l)
        self.assertAlmostEqual(next(g), 1.0)
        self.assertAlmostEqual(next(g), 0.667, places=3)
        self.assertAlmostEqual(next(g), 0.867, places=3)
        self.assertAlmostEqual(next(g), 0.724, places=3)
        self.assertAlmostEqual(next(g), 0.835, places=3)
        self.assertAlmostEqual(l.list_leibniz(6)[5], 0.744, places=3)
        self.assertAlmostEqual(l.list_leibniz(500)[499], math.pi / 4, places=3)

    # csv to dict
    def test_csv_to_dict(self):
        ans = [{'gender': 'female', 'age': '22', 'student': 'Anne'},
               {'gender': 'male', 'age': '21', 'student': 'John'}]
        self.assertEqual(csv_to_dict('test.csv'), ans)

        with self.assertRaises(Exception, msg='No such file'):
            csv_to_dict('made_up_file.csv')

        with self.assertRaises(InvalidFormatException):
            csv_to_dict('bad.csv')

    # encode decode text
    def test_encode_text(self):
        CODE_FILE = 'code.txt'
        BAD_CODE_FILE = 'bad_code.txt'
        FILE_1 = 'in.txt'
        FILE_2 = 'out.txt'
        FILE_3 = 'new_in.txt'

        encode_text(CODE_FILE, FILE_1, FILE_2)
        with open(FILE_2) as f:
            self.assertEqual(f.read(), 'Twbz arillig, bnd tho zlithy tevoz\n')

        with self.assertRaises(InvalidFormatException):
            encode_text(BAD_CODE_FILE, FILE_1, FILE_2)

        with self.assertRaises(Exception, msg='No such file'):
            encode_text(BAD_CODE_FILE, FILE_1, FILE_2)

        encode_text(CODE_FILE, FILE_1, FILE_2)
        decode_text(CODE_FILE, FILE_2, FILE_3)
        with open(FILE_3) as f:
            self.assertEqual(f.read(), 'Twas brillig, and the slithy toves\n')


def main():
    unittest.main()

if __name__ == '__main__':
    main()
